﻿using UnityEngine;
using System.Collections;

public class BossBullet : MonoBehaviour
{
    public float force = 750f;

    void Start()
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        Transform godPos = GameObject.FindGameObjectWithTag("GOD").transform;

        if(rb != null && godPos != null)
        {
            Vector3 dir = transform.position - godPos.position;
            rb.AddForce(dir * force);
        }
    }
}
