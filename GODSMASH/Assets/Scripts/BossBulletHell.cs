﻿using UnityEngine;
using System.Collections;

public class BossBulletHell : MonoBehaviour
{
    public GameObject[] waves;
    public float attackRate = 1.0f;
    private float lastAttack = 0;


	void Update ()
    {
	    if(Time.timeSinceLevelLoad > lastAttack + attackRate)
        {
            Instantiate(waves[Random.Range(0, waves.Length)], transform.position, transform.rotation);
            lastAttack = Time.timeSinceLevelLoad;
        }
	}
}
