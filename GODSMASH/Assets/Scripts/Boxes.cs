﻿using UnityEngine;
using System.Collections;

/*this is a script that is placed on all boxes that are thrown around by the player and the GOD, it decides
whether or not something uses regular gravity or the exact opposite of it by looking at the boxes tag, 
if we want the box to be one way or another we just need to change it's tag*/

public class Boxes : MonoBehaviour
{

    public GameObject gravController;                                               //This is the controller that we are looking at to change gravity, initialised in the inspector
    public GravitySwitch gravScript;                                                //the instance of the gravSwitch cript that we are looking to in the scene (this is attached to the controller)
    public Rigidbody rb;                                                            //the rigid body that is on this object

    public bool gravity = false;                                                    //if true this object follows gravity|If false this object does the opposite of gravity
    public bool coolDown = false;                                                   // if this is true, collision with GOD will not be registered

    public Material green;                                                          //The material that signifies this object being subject to normal gravity
    public Material red;                                                            //The material that signifies this object being -gravity

    public float coolDownTimer = 1f;                                                //the timer that is counted down to reinitialize collisions with GOD

    // Use this for initialization
    void Start()
    {

        gravScript = gravController.GetComponent<GravitySwitch>();                  //initialises the instance of gravscript from the grav controller object that we assigned in the inspector
        rb = this.gameObject.GetComponent<Rigidbody>();                             //the rigidbody attached to the same gamObject as this script

    }

    // Update is called once per frame
    void Update()
    {

        if (gameObject.tag == "gravity")                                            //if this objects tag is gravity:
        {
            gravity = true;                                                         //this object will follow gravity


            rb.AddForce(Physics.gravity, ForceMode.Acceleration);                   //take gravity and apply it to this rigidbody in a way that ignores it's mass
            rb.useGravity = false;                                                  //Make this object's rigidbody ignore gravity
                                                                                    //  This line reverses gravity on this object as long as gravity is unchecked on rigidbody

            if (gameObject.GetComponent<MeshRenderer>().material != green)          //check if this object is using the right material for gravity
            {
                gameObject.GetComponent<MeshRenderer>().material = green;       //if not the right material, addign it
            }


        }

        else if (gameObject.tag == "-gravity")                                           //if -gravity is this object's tag
        {
            gravity = false;                                                        //flick thee gravity booli as false

            rb.AddForce(-Physics.gravity, ForceMode.Acceleration);                  //apply gravitational force in the opposite direction of gravity
            rb.useGravity = false;                                                  //make sure this gameObject's rigidbody ignores natural gravity
                                                                                    //  This line reverses gravity on this object as long as gravity is unchecked on rigidbody

            if (gameObject.GetComponent<MeshRenderer>().material != red)            //check if the object uses the correct material
            {
                gameObject.GetComponent<MeshRenderer>().material = red;         //change it if not
            }


        }


        if (coolDown)                                                               //if this bool is true:
        {
            coolDownTimer -= Time.deltaTime;                                        //start a timer that will deplete

            if (coolDownTimer < 0)                                                  // when the timer reaches 0
            {
                coolDown = false;                                                   //reset coolDown bool
                coolDownTimer = 20f;                                                //reset the cooldown timer
            }
        }

    }

    void OnCollisionEnter(Collision collision)                                      //when this object collides with something:
    {

        foreach (ContactPoint contact in collision.contacts)                        //for each thing that it collides with:
        {


            if (contact.otherCollider.gameObject.tag == "GOD" && coolDown == false) //If the collider is on an object is tagged as "GOD" & the coold down is false:
            {

                coolDown = true;                                                    //set coolDown to true

                if (gravity)                                                        //if gravity is true
                {
                    gameObject.tag = "-gravity";                                    //change this objects tag to -gravity
                    gameObject.GetComponent<MeshRenderer>().material = red;         //change the material to red
                }

                else if (!gravity)                                                  //if gravity is false
                {
                    gameObject.tag = "gravity";                                     //change the tag to gravity
                    gameObject.GetComponent<MeshRenderer>().material = green;       //change the material back to green
                }

            }


        }
    }
}
