﻿using UnityEngine;
using System.Collections;

public class BreakOut : MonoBehaviour
{
    public ReverseGravity rg;

    // Use this for initialization
    void Start()
    {
        rg = gameObject.GetComponent<ReverseGravity>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision col)
    {
        foreach (ContactPoint contact in col.contacts)
        {
            if (contact.otherCollider.tag == "wall" && contact.otherCollider.gameObject.GetComponent<Rigidbody>() == null)
            {
             
                GameObject p = contact.otherCollider.gameObject;
                p.transform.SetParent(null);

                ReverseGravity wallRG = p.AddComponent<ReverseGravity>();
                //p.AddComponent<BreakOut>();
                wallRG.gravController = rg.gravController;
                    wallRG.green = rg.green;
                    wallRG.red = rg.red;


                    if (p.GetComponent<Rigidbody>() == null && gameObject.tag == "gravity")
                    {
                        Rigidbody rb = p.AddComponent<Rigidbody>();
                        rb.useGravity = false;
                        p.tag = "-gravity";
                        rb.mass = gameObject.GetComponent<Rigidbody>().mass * 5f;
                        gameObject.tag = "-gravity";
                    p.GetComponent<MeshRenderer>().material = rg.red;
                }

                    else if (p.GetComponent<Rigidbody>() == null && gameObject.tag == "-gravity")
                    {
                        Rigidbody rb = p.AddComponent<Rigidbody>();
                        rb.useGravity = false;
                        p.tag = "gravity";
                        rb.mass = gameObject.GetComponent<Rigidbody>().mass * 5f;
                        gameObject.tag = "gravity";
                    p.GetComponent<MeshRenderer>().material = rg.green;
                }

            }
                //else
                //{
                //    GameObject p = contact.otherCollider.transform.parent.gameObject;
                //    p.transform.SetParent(null);
                //    if (p.GetComponent<Rigidbody>() == null)
                //    {
                //        Rigidbody rb = p.AddComponent<Rigidbody>();
                //        rb.useGravity = false;
                //        //rb.drag = dragAddedCaolan;
                //    }

                //}
            }
        }

}

