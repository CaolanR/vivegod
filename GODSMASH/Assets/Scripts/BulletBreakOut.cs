﻿using UnityEngine;
using System.Collections;
using Chronos;

public class BulletBreakOut : MonoBehaviour
{
    public ReverseGravity rg;
    public GameObject refgo;

    public GameObject particlePrefab;

    public float psRadius = 1f;
    public HighScores gameManager;

    public bool attachPS;                       //whether or not the bulletBreakOutScript will attach Praticle system Objects to objects that it collides with

    void Start()
    {
        refgo = GameObject.FindGameObjectWithTag("refrg");
        rg = refgo.GetComponent<ReverseGravity>();       
    }

    void OnCollisionEnter(Collision col)
    {
        foreach (ContactPoint contact in col.contacts)
        {
            if (contact.otherCollider.tag == "wall" && contact.otherCollider.gameObject.GetComponent<Rigidbody>() == null)  //for Wall objects
            {
                Destroy(gameObject.GetComponent<collisionEffects>());                                                           //Fix that prevents bricks instantly changing out of player control upon shooting them (there is probably a more efficient way of doing this(like make both scripts on the bullet methods to be called instead of always active etc.))

                GameObject p = contact.otherCollider.gameObject;                                                                //The game object that is hit in the collision
                p.transform.SetParent(null);

                Rigidbody rb = p.AddComponent<Rigidbody>();                                                                     //Add Rigidbody object
                rb.useGravity = false;
                rb.mass = 1e+09f;                
              
                ReverseGravity wallRG = p.AddComponent<ReverseGravity>();                                                           //Add Reverse Gravity script         
                wallRG.gravController = rg.gravController;
                wallRG.green = rg.green;
                wallRG.red = rg.red;
                wallRG.hologram1 = rg.hologram1;
                wallRG.hologram2 = rg.hologram2;
                wallRG.psTime = rg.psTime;
                wallRG.psDefault = rg.psDefault;
                wallRG.tc = rg.tc;
                wallRG.breakOutCoolDown = true;                                                                                     //makes sure bricks can come out of the wall immediately

                Timeline tl = p.AddComponent<Timeline>();                                                                           //Add TimeLine
                tl.mode = TimelineMode.Global;
                tl.globalClockKey = "Boxes";
                tl.rewindable = true;
                tl.recordingDuration = 30f;
                tl.recordingInterval = 1f;

                if (p.GetComponent<MeshCollider>() != null)                                                                         //Some bricks have Box Colliders, this is needed so we don't spam the console with crap
                {
                    p.GetComponent<MeshCollider>().convex = true;                                                                           //Make the opposite mesh collider Convex
                }

                if (p.GetComponent<DotProductTest>().IsFacing() == false)
                {
                    p.tag = "-gravity";
                    //p.GetComponent<MeshRenderer>().material = rg.red;
                }

                else if (p.GetComponent<DotProductTest>().IsFacing() == true)
                {
                    p.tag = "gravity";
                    //p.GetComponent<MeshRenderer>().material = rg.green;
                }

                TrailRenderer tr = p.AddComponent<TrailRenderer>();                                                                     //adds a trail renderer
                tr.startWidth = refgo.GetComponent<TrailRenderer>().startWidth;
                tr.endWidth = refgo.GetComponent<TrailRenderer>().endWidth;
                tr.time = refgo.GetComponent<TrailRenderer>().time;

                if (p.GetComponent<Timeline>() != null)                                                                                 
                {
                    VelocityTest vt = p.AddComponent<VelocityTest>();                                                                   //Adds a Velocity test Script
                }
                //vt.contInput = refgo.GetComponent<VelocityTest>().contInput;

                //if (wallRG.ps() != null)
                //{
                //    ParticleSystem ps = wallRG.ps();                                                                                        //Add Particle System For time
                //    var em = ps.emission;
                //    em.enabled = true;
                //    psRadius /= p.transform.localScale.x;                                                                                   //adjusts the particleObject's scale to match it's parent
                //    print("psRadius = " + psRadius);
                //    var sh = ps.shape;
                //    sh.radius = psRadius;
                //}

                //if (attachPS)
                //{
                //    GameObject particleChild = Instantiate(particlePrefab, Vector3.zero, Quaternion.identity) as GameObject;            //add particle child object
                //    particleChild.transform.parent = p.transform;
                //    particleChild.transform.localPosition = Vector3.zero;
                //    particleChild.transform.localScale = Vector3.one;

                //    //if (p.GetComponent<Timeline>() != null)                                                                             //must check if the parent object has a timline before adding a timline child
                //    //{
                //    //    TimelineChild pc = particleChild.AddComponent<TimelineChild>();                                                 //add a timelinechild component to the particle object
                //    //}
                //}
            }

            else if(contact.otherCollider.tag == "projectile")                                                                      // If god's projectile is the object hit
            {
                GameObject p = contact.otherCollider.gameObject;                                                                            //The game object that is hit in the collision
                Destroy(p.GetComponent<Projectile>());

                if (attachPS)
                {
                    GameObject particleChild = Instantiate(particlePrefab, Vector3.zero, Quaternion.identity) as GameObject;                         //add particle child object
                    particleChild.transform.parent = p.transform;
                    particleChild.transform.localPosition = Vector3.zero;
                    particleChild.transform.localScale = Vector3.one;

                    //if (p.GetComponent<Timeline>() != null)                                                                                          //must check if the parent object has a timline before adding a timline child
                    //{
                    //    TimelineChild pc = particleChild.AddComponent<TimelineChild>();                                                                     //add a timelinechild component to the particle object
                    //}
                }

                ReverseGravity wallRG = p.AddComponent<ReverseGravity>();                                                                   //Add Reverse Gravity script         
                wallRG.gravController = rg.gravController;
                wallRG.green = rg.green;
                wallRG.red = rg.red;
                wallRG.hologram1 = rg.hologram1;
                wallRG.hologram2 = rg.hologram2;
                wallRG.psTime = rg.psTime;
                wallRG.psDefault = rg.psDefault;
                wallRG.tc = rg.tc;
                wallRG.breakOutCoolDown = true;

                if (wallRG.ps() != null)
                {
                    ParticleSystem ps = wallRG.ps();                                                                                                //Add Particle System For time
                    var em = ps.emission;
                    em.enabled = true;
                    psRadius /= p.transform.localScale.x;                                                                                           //adjusts the particleObject's scale to match it's parent
                    print("psRadius = " + psRadius);
                    var sh = ps.shape;
                    sh.radius = psRadius;
                }

                Rigidbody rb = p.GetComponent<Rigidbody>();                                                                                 //grab the opposite RigidBody
                rb.useGravity = false;
                rb.mass = 1e+09f;
                rb.isKinematic = false;

                p.tag = "gravity";

                p.GetComponent<MeshRenderer>().material = rg.green;

                Timeline tl = p.AddComponent<Timeline>();                                                                                   //Add TimeLine

                tl.mode = TimelineMode.Global;
                tl.globalClockKey = "Boxes";
                tl.rewindable = true;
                tl.recordingDuration = 30f;
                tl.recordingInterval = 1f;               
            }
        }
    }
}


