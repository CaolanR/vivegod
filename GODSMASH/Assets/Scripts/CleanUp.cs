﻿using UnityEngine;
using System.Collections;

public class CleanUp : MonoBehaviour {
//for use on particle effects left over from shooting
	void Update () {
        Destroy(this.gameObject, 5f);
	}
}
