﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*This script is placed on the GOD, it registers damage and kills him when it reaches a certain threshold*/

public class Death : MonoBehaviour {

    public Material damage;                                                     //A material assigned in the inspector that is flashed briefly after the GOD is damaged
    public Material GODMat;                                                     //The normal material assigned to the god, assigned in the inspector

    public int health = 100;                                                    //GODS Health
    public float timer = 1f;                                                    //The timer that is decreased after he is hit by something
   
    public bool damaged = false;                                                //this is used to know if the GOD can be damaged again

    public Text healthText;                                                     //This is the text box that displays in front of the god

    void Update()
    {
        if (damaged)                                                            //if "Damaged" is true
        {
            timer -= Time.deltaTime;                                            //timer is decreased frame by frame by time.DeltaTime
            health -= 1;                                                        //health is decreased frame by frame by 1
            if (timer < 0)                                                      //when time reaches 0..
            {
                gameObject.GetComponent<MeshRenderer>().material = GODMat;      //switch the material to "GODMat"
                damaged = false;                                                //reset damaged to false
                timer = 1f;                                                     //reset timer to 1;
                
            }
        }

        healthText.text = health.ToString();                                    //makes the text in front of GOD display his health

        if(health < 0)                                                          //if GOD's health reaches below 0, Kill him
        {
            Dead();
        }

    }

    void OnCollisionEnter(Collision collision)                                  //when a collision is registered
    {
        foreach (ContactPoint contact in collision.contacts)                    //for each contact collider
        {


            if (contact.otherCollider.gameObject.tag == "gravity" || contact.otherCollider.gameObject.tag == "-gravity")    //check if the tags are either gravity or -gravity
            {
                Damaged();
            }
        }
    }

    public void Damaged()                                                       //function that is called when GOD's "damaged" bool is true
    {
        
        gameObject.GetComponent<MeshRenderer>().material = damage;              //changes the material on GOD to the "damaged" material
        damaged = true;                                                         //flicks the damaged bool to true which starts the timer variable declining in update
        
        return;
    }

    public void Dead()
    {
        Destroy(gameObject);                                                    //called when the GOD's health reached 0
    }
}
