﻿using UnityEngine;
using System.Collections;

public class DebugVolocity : MonoBehaviour {

    public Vector3 grav;
    public Vector3 oppGrav;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        grav = Physics.gravity;
        oppGrav = -Physics.gravity;

        Debug.Log("the cross product = " + Vector3.Cross(grav, oppGrav));

    }
}
