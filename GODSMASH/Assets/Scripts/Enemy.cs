﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public float lerpSp = 0f;                   //increase this to change the speed of the slerp
    public float lerpBase = .01f;               //The figure which lerpSp is reset to after firing
    public float lerpMultiplier;                //the amount by which movement speed of the god is boosted each time he is hit by something

    public float speed;
    public float fireRate;
    private float lastFired = 0;
    private Transform target;

    public Quaternion rotTo;

    public GameObject projectile;
    public GameObject projectileOrigin;
    public GameObject head;                                                 //used to find the cross product vector

    public TimeControl timeController;

    public bool hit = false;
    public bool fire = false;
    public bool cacheRot = true;
    public bool testCache = false;          //testing the cache idea, remove in builds

    public Death death;

    public Animator anim;

    public AudioClip drum;

    void Start()
    {
        target = Camera.main.transform;
        anim = gameObject.GetComponent<Animator>();
        death = gameObject.GetComponent<Death>();
    }


    void Update()
    {
        //testing Cache Idea
        if (testCache)
        {
            if (cacheRot)
            {
                rotTo = Quaternion.FromToRotation((transform.parent.position - gameObject.transform.position),                      //creates a rotation that will be used to bring the god from his current position to a new one by rotating it's parent (acts as a pivot point)
                        (Vector3.Cross(Physics.gravity, head.transform.forward)));
                cacheRot = false;
                testCache = false;
            }
            //else
            //{
            //    testCache = false;
            //}
        }



        transform.LookAt(target.position);                                  //looks at the player

        if (hit)
        {
            transform.parent.rotation = Quaternion.Slerp(transform.parent.transform.rotation, rotTo, lerpSp * Time.deltaTime);  //slerps from current rotation to the new rotation

            fire = true;

            if (!timeController.playing)
            {
                timeController.playing = true;
            }

            if (!death.damaged && Vector3.Angle(transform.parent.rotation.eulerAngles, rotTo.eulerAngles) < .01f)               //checks against the cool down time in the death script, theres is a cool down after each collision that hurts the god
            {
                hit = false;
                lerpSp = lerpBase;
                cacheRot = true;
            }
        }

        else if (Time.timeSinceLevelLoad > fireRate + lastFired && fire)            //legacy shooting, needs replacement
        {
            //anim.SetTrigger("shoot");
            print("Fire!");
            fire = false;
        }
    }

    void Fire()
    {
        Instantiate(projectile, projectileOrigin.transform.position, projectileOrigin.transform.rotation);
        lastFired = Time.timeSinceLevelLoad;
        gameObject.GetComponent<AudioSource>().PlayOneShot(drum, 5);
        lerpSp = lerpBase;
    }

    void OnCollisionEnter(Collision collision)                                      //when this object collides with something:
    {
        foreach (ContactPoint contact in collision.contacts)                        //for each thing that it collides with:
        {
            hit = true;                                                             //when this is true, do all the stuff that happens when the god has been hit
            lerpSp += lerpMultiplier * lerpSp;                                      //The amount at which movement speed (slerp) is multiplied every time he is hit consecutively

            if (cacheRot)
            {
                rotTo = Quaternion.FromToRotation((transform.parent.position - gameObject.transform.position),                      //creates a rotation that will be used to bring the god from his current position to a new one by rotating it's parent (acts as a pivot point)
                        (Vector3.Cross(Physics.gravity, head.transform.forward)));
                cacheRot = false;
            }
        }
    }
}
