﻿using UnityEngine;
using System.Collections;

public class GrabCube : MonoBehaviour {

    private Transform grabbedObj;
    private LineRenderer line;

	// Use this for initialization
	void Start () {
        line = gameObject.GetComponent<LineRenderer>();
	}

    void Update()
    {
        
        if (gameObject.GetComponent<WandControl>().triggerButtonDown)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit, 200))
            {

                Debug.Log("HIT");

                if (hit.collider.gameObject.tag == "Physics")
                {
                    Debug.Log("Grabbed Physics object");
                    grabbedObj = hit.collider.gameObject.transform;
                    grabbedObj.parent = transform;
                    if(grabbedObj.gameObject.GetComponent<Rigidbody>() != null)
                    {
                        grabbedObj.gameObject.GetComponent<Rigidbody>().useGravity = false;
                    }
                }
            }
       }

        if (gameObject.GetComponent<WandControl>().triggerButtonUp)
        {
            if(grabbedObj != null)
            {
                grabbedObj.parent = null;
                

                if (grabbedObj.gameObject.GetComponent<Rigidbody>() != null)
                {
                    grabbedObj.gameObject.GetComponent<Rigidbody>().useGravity = true;
                }

                grabbedObj = null;
            }
        }

    }

    void LateUpdate()
    {
        if (grabbedObj != null)
        {
            line.enabled = true;
            line.SetPosition(0, transform.position);
            line.SetPosition(1, grabbedObj.position);
        }
        else
        {
            line.enabled = false;
        }
    }
}
