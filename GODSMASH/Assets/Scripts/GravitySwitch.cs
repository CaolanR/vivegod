﻿using UnityEngine;
using System.Collections;

public class GravitySwitch : MonoBehaviour {

    public Vector3 gravUp = new Vector3(0,10,0);                        
    public Vector3 gravDown = new Vector3(0, -10, 0);

    public Vector3 startPos;
    public Vector3 currentPos;
    public Vector3 direction;

    public GameObject controllerCube;
    public float mag;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (gameObject.GetComponent<WandControl>().triggerButtonDown)
        {
            startPos = controllerCube.transform.position;
        }

        if (gameObject.GetComponent<WandControl>().triggerButtonPressed)
        {
            currentPos = controllerCube.transform.position;
            direction = (startPos - currentPos).normalized;
            Physics.gravity = -direction;
            mag = (startPos - currentPos).magnitude * 2f;
        }

        if (gameObject.GetComponent<WandControl>().triggerButtonUp)
        {
            direction = direction * mag * 10f;
        }

    }
}

//holding down the trigger but not moving should build up a multiplier that is applied to mag when untriggered
