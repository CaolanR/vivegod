﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class HighScores : MonoBehaviour {

    public bool runComplete;
    public string highscorePos;
    public static float score;
    public int temp;
    public GameObject scoreTable;
    public GameObject cam;

    public TimeControl timeController;

    //chris's script
    public Text[] scoreText;

	void Start () {
        
	}
	
	void Update () {
	
	}

   public void SetHighScore(float myScore)
    {              
            string scoreKey = "HScore";
            float newScore = myScore;            
            float oldScore;

            for (int i = 0; i< scoreText.Length; i++)
            {
                if (PlayerPrefs.HasKey(scoreKey + i))
                {
                    if (PlayerPrefs.GetFloat(scoreKey + i) < newScore)
                    {
                        oldScore = PlayerPrefs.GetFloat(scoreKey + i);                   
                        PlayerPrefs.SetFloat(scoreKey + i, newScore);
                        newScore = oldScore;
                    }
                }
                else
                {
                    PlayerPrefs.SetFloat(scoreKey + i, newScore);
                    newScore = 0;
                }
            }       
        runComplete = true;
        score = timeController.currentScore;
    }

   public void ShowHiScores()
    {
        string scoreKey = "HScore";
        scoreTable.SetActive(true);
        for (int i = 0; i < scoreText.Length; i++)
        {
            scoreText[i].text = PlayerPrefs.GetFloat(scoreKey + i).ToString("F2");
        }
    }

    public void replay()
    {
        cam.GetComponent<ColorCorrectionCurves>().saturation = 1f;
        scoreTable.SetActive(false);
    }
}
