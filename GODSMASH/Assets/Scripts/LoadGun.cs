﻿using UnityEngine;
using System.Collections;
using VRTK;

public class LoadGun : MonoBehaviour {

    public Animator anim;
    public ParticleSystem cockSparks;
    public ParticleSystem barrelSparks;
    public ParticleSystem barrelSmoke;

    public VRTK_ControllerEvents_ListenerExample controller;
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    public void unLoad()
    {
        anim.SetBool("loaded", false);
    }

    public void cockSparksPe()
    {
        cockSparks.Play();
        //barrelSmoke.Play();
        barrelSparks.Play();
    }
    public void barrelSparksPe()
    {
        barrelSparks.Play();
    }
    public void barrelSmokePe()
    {
        barrelSmoke.Play();
    }

    //public void CanLoad()
    //{
    //    if(controller.canload){
    //        controller.canload = false;
    //    }
    //    else if (!controller.canload)
    //    {
    //        controller.canload = true;
    //    }
    //}
    
}
