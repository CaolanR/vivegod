﻿using UnityEngine;
using System.Collections;

public class PlayAgain : MonoBehaviour {

    public HighScores hs;

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            hs.replay();
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
