﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Projectile : MonoBehaviour
{
    public Vector3 target;              //not used?

    public int speed;
    public float power = 100f;
    Rigidbody rb;
    public GameObject g;
    public TimeControl TimeController;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        TimeController = GameObject.FindGameObjectWithTag("rightC").GetComponent<TimeControl>();
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "MainCamera")
        {
            col.gameObject.GetComponent<ColorCorrectionCurves>().saturation = 0f;
            Destroy(gameObject);
            TimeController.playing = false;
        }   
    }

    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            g = contact.otherCollider.gameObject;

            if (g.tag == "gravity")
            {
                ReverseGravity rg = gameObject.GetComponent<ReverseGravity>();
                gameObject.tag = "-gravity";
                rb.isKinematic = false;
                
                Destroy(this);

                if (rg == null)
                {
                    AddGravity();
                }
            }

            else if (g.tag == "-gravity")
            {
                ReverseGravity rg = gameObject.GetComponent<ReverseGravity>();
                gameObject.tag = "gravity";
                rb.isKinematic = false;
                
                Destroy(this);

                if (rg == null)
                {
                    AddGravity();               
                }
            }
        }
    }

    public void AddGravity()
    {
        ReverseGravity rg = gameObject.AddComponent<ReverseGravity>();
        rg.gravController = g.GetComponent<ReverseGravity>().gravController;
        rg.green = g.GetComponent<ReverseGravity>().green;
        rg.red = g.GetComponent<ReverseGravity>().red;
        rg.coolDownTimer = g.GetComponent<ReverseGravity>().coolDownTimer;

        if (g.tag == "gravity")
        {
            gameObject.GetComponent<MeshRenderer>().material = rg.red;
        }

        else if (g.tag == "-gravity")
        {
            gameObject.GetComponent<MeshRenderer>().material = rg.green;
        }
    }
}
