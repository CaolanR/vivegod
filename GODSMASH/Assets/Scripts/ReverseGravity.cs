﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using Chronos;

/*this is a script that is placed on all boxes that are thrown around by the player and the GOD, it decides
whether or not something uses regular gravity or the exact opposite of it by looking at the boxes tag, 
if we want the box to be one way or another we just need to change it's tag*/

public class ReverseGravity : MonoBehaviour {

    public GameObject gravController;                                               //This is the controller that we are looking at to change gravity, initialised in the inspector
    public GravitySwitch gravScript;                                                //the instance of the gravSwitch cript that we are looking to in the scene (this is attached to the controller)
    public Rigidbody rb;                                                            //the rigid body that is on this object
    public TimeControl tc;                                                          //the time control script that controls the flow of time on the "boxes" clock
    public TimeControl timeController;                                              //the timecontroller that keeps track of the players score (time)

    public bool gravity = false;                                                    //if true this object follows gravity|If false this object does the opposite of gravity
    public bool coolDown = false;                                                   // if this is true, collision with GOD will not be registered
    public bool breakOutCoolDown = false;                                           //For freeing the bricks from the wall, linked to "BulletBreakOut"

    public Material green;                                                          //The material that signifies this object being subject to normal gravity
    public Material red;                                                            //The material that signifies this object being -gravity
    public Material psTime;                                                         //the material colour for particle system
    public Material psDefault;                                                      //2nd material for particle system
    public Material hologram1;
    public Material hologram2;

    public float coolDownTimer = 1f;                                                //the timer that is counted down to reinitialize collisions with GOD
    public float breakOutTimer = 1f;                                                //the timer depleted to reinitialize the meshCollider on a piece of wall that is removed

	void Start () {
        gravScript = gravController.GetComponent<GravitySwitch>();                  //initialises the instance of gravscript from the grav controller object that we assigned in the inspector
        rb = this.gameObject.GetComponent<Rigidbody>();                             //the rigidbody attached to the same gamObject as this script

        if (gameObject.tag == "gravity")
        {
            SetMat();              //changes the colour of trhe brick to match it's tag in the inspector when game starts

            if (ps() != null)
            {
                ps().startColor = green.color;                                  //for particle system
            }
        }
        else if (gameObject.tag == "-gravity")
        {
            SetMat();

            if (ps() != null)                                                   //for particle system
            {
                ps().startColor = red.color;
            }
        }        
    }
    void Update () {

        if (gameObject.tag == "gravity")                                            //if this objects tag is gravity:
        {
            gravity = true;                                                         //this object will follow gravity
            if (gravity)
            {
                rb.AddForce(Physics.gravity, ForceMode.Acceleration);                   //take gravity and apply it to this rigidbody in a way that ignores it's mass
                rb.useGravity = false;                                                  //Make this object's rigidbody ignore gravity
            }
        }
        else if (gameObject.tag == "-gravity")                                           //if -gravity is this object's tag
        {
            gravity = false;                                                        //flick thee gravity booli as false

            if (!gravity)
            {
                rb.AddForce(-Physics.gravity, ForceMode.Acceleration);                  //apply gravitational force in the opposite direction of gravity
                rb.useGravity = false;                                                  //make sure this gameObject's rigidbody ignores natural gravity                                                                                   
            }
        }
        if (coolDown)                                                               //if this bool is true:
        {
            coolDownTimer -= Time.deltaTime;                                        //start a timer that will deplete
            if (coolDownTimer < 0)                                                  // when the timer reaches 0
            {
                coolDown = false;                                                   //reset coolDown bool
                coolDownTimer = 2f;                                                //reset the cooldown timer
            }
        }

        if (breakOutCoolDown)                                                              
        {
            breakOutTimer -= Time.deltaTime;
            gameObject.GetComponent<MeshCollider>().enabled = false;
            if (breakOutTimer < 0)                                                  // when the timer reaches 0
            {
                breakOutCoolDown = false;                                                   //reset coolDown bool
                breakOutTimer = 1f;                                                //reset the cooldown timer
            }
        }
        else if (!breakOutCoolDown)
        {
            if (gameObject.GetComponent<MeshCollider>() != null)
            {
                gameObject.GetComponent<MeshCollider>().enabled = true;
            }
        }

        //if (psr() != null)
        //{
        //    if (tc.timeStopped && gameObject)
        //    {
        //        psr().material = psTime;
        //    }                                                                                  //changes the color of the active particles on the game object by swapping their renderer's material
        //    else if (!tc.timeStopped)
        //    {
        //        psr().material = psDefault;
        //    }
        //}
    }
    void OnCollisionEnter(Collision collision)                                      //when this object collides with something:
    {
        foreach (ContactPoint contact in collision.contacts)                            //for each thing that it collides with:
        {
            if (contact.otherCollider.gameObject.tag == "GOD" && coolDown == false)         //If the collider is on an object is tagged as "GOD" & the cool down is false:
            {
                coolDown = true;                                                                //set coolDown to true
                if (gravity)                                                                    //if gravity is true
                {
                    gameObject.tag = "-gravity";                                                    //change this objects tag to -gravity
                    SetMat();                                                                       //change the material to red
                }
                else if (!gravity)                                                                  //if gravity is false
                {
                    gameObject.tag = "gravity";                                                     //change the tag to gravity
                    SetMat();                                                                       //change the material back to green
                }
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "MainCamera")
        {
            col.gameObject.GetComponent<ColorCorrectionCurves>().saturation = 0f;
            Destroy(gameObject);
            tc.playing = false;
        }
    }

    //methods to change the time particle effects
    public ParticleSystemRenderer psr() {                                       //For finding the particle RENDERER on an object
     if (gameObject.GetComponent<ParticleSystemRenderer>() != null)
            {
                return (gameObject.GetComponent<ParticleSystemRenderer>());
            }
     else if (gameObject.GetComponent<ParticleSystemRenderer>() == null && transform.childCount != 0)
            {
                return (gameObject.transform.GetChild(0).GetComponent<ParticleSystemRenderer>());
            }
     else
        {
            return (null);
        }
    }

    public ParticleSystem ps()                                                      //For finding the particle system on an object
    {
        if (gameObject.GetComponent<ParticleSystem>() != null)                      //if the gameobject has a particle system componant, assign it
        {
            return (gameObject.GetComponent<ParticleSystem>());
        }
        else if (gameObject.GetComponent<ParticleSystem>() == null && transform.childCount != 0)                 //if the gameobject does not have a particle system componant assign the one on it's first child
        {
            return (gameObject.transform.GetChild(0).GetComponent<ParticleSystem>());
        }
        else
        {
            return (null);
        }
    }

    public void SetMat()
    {
        //print("SetMat called");

        if (gameObject.tag == "gravity" && gameObject.GetComponent<Timeline>().globalClockKey == "Boxes")
        {
            gameObject.GetComponent<MeshRenderer>().material = hologram1;
            gameObject.GetComponent<TrailRenderer>().material = hologram1;
        }
        else if (gameObject.tag == "-gravity" && gameObject.GetComponent<Timeline>().globalClockKey == "Boxes")
        {
            gameObject.GetComponent<MeshRenderer>().material = hologram2;
            gameObject.GetComponent<TrailRenderer>().material = hologram2;
        }
        else if (gameObject.tag == "gravity" && gameObject.GetComponent<Timeline>().globalClockKey != "Boxes")
        {
            gameObject.GetComponent<MeshRenderer>().material = green;
        }
        else if (gameObject.tag == "-gravity" && gameObject.GetComponent<Timeline>().globalClockKey != "Boxes")
        {
            gameObject.GetComponent<MeshRenderer>().material = red;
            //print("Dick");
        }
    }
}
