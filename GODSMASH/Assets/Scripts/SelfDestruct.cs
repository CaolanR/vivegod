﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour
{
    public float timer = 1.0f;
	
	// Update is called once per frame
	void Update ()
    {
        Destroy(gameObject, timer);
    }
}
