﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{

    public GameObject Bullet_Emitter;


    public GameObject Bullet;


    public float Bullet_Forward_Force;

    public AudioSource gunShot;

    void Start()
    {

    }


    public void Fire()
    {
            gunShot.Play();

            GameObject Temporary_Bullet_Handler;
            Temporary_Bullet_Handler = Instantiate(Bullet, Bullet_Emitter.transform.position, Bullet_Emitter.transform.rotation) as GameObject;

            Rigidbody Temporary_RigidBody;
            Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody>();


            Temporary_RigidBody.AddForce(-Bullet_Emitter.transform.forward * Bullet_Forward_Force);

            Destroy(Temporary_Bullet_Handler, 10.0f);
    }
}
