﻿using UnityEngine;
using System.Collections;
using Chronos;

//this script changes an objects velocity suddelny upon resuming time flow IF gravity has been changed in between
public class VelocityTest : MonoBehaviour {

    public Rigidbody rb;
    public Timeline tl;

    //public Clock clock;

    public VRTK_ControllerEvents_ListenerExample contInput;

    public Vector3 frozenVel;
    public Vector3 rbVel;
    public Vector3 gravNorm;


    public float frozenVelMag;
    public bool canVel;
    public bool change = false;

    public static bool controllerOn = false;

	void Start () {
        rb = gameObject.GetComponent<Rigidbody>();
        tl = gameObject.GetComponent<Timeline>();
        //clock = Timekeeper.instance.Clock("Boxes");
        contInput = GameObject.FindGameObjectWithTag("refrg").GetComponent<VelocityTest>().contInput;
    }

    void Update() {

        //records and implements the velocity change that is to happen if time is currently frozen
        if (gameObject.GetComponent<Timeline>().globalClockKey == "Boxes")
        {
            if (!contInput.timeFreeze)
            {
                rbVel = tl.rigidbody.velocity;
            }
            if (contInput.timeFreeze)
            {
                frozenVel = rbVel;
                frozenVelMag = frozenVel.magnitude;

                canVel = true;
                gravNorm = Physics.gravity.normalized;
            }
            if (!contInput.timeFreeze && canVel)
            {
                canVel = false;
                change = true;
            }

            //if (change && gameObject.GetComponent<Timeline>().rigidbody.velocity.x >= frozenVel.x) //this check makes sure the Rigidbody is engaged on the timeline again, as blocks that had stuff added at runtime weren't getting changed
            //{
            //    ChangeVelocity();
            //    change = false;
            //}

            //if (change) //for debugging
            //{
            //    print("change is on, " + gameObject.name + "with tag: " + gameObject.tag + "has a rigidbody.velocity.x value of: " + gameObject.GetComponent<Timeline>().rigidbody.velocity.x);
            //}
        }

        if (change && gameObject.GetComponent<Timeline>().rigidbody.velocity.x >= frozenVel.x) //this check makes sure the Rigidbody is engaged on the timeline again, as blocks that had stuff added at runtime weren't getting changed
        {
            ChangeVelocity();
            change = false;
        }

    }
    //call this to change velocity
        public void ChangeVelocity()
    {
        print("ChangeVelocity() called");
        if (gameObject.tag == "gravity")
        {
            gameObject.GetComponent<Timeline>().rigidbody.velocity = gravNorm * frozenVelMag;
            print(gameObject.name + "New velocity is = " + gameObject.GetComponent<Timeline>().rigidbody.velocity);
        }
        else if (gameObject.tag == "-gravity")
        {
            gameObject.GetComponent<Timeline>().rigidbody.velocity = -gravNorm * frozenVelMag;
            print(gameObject.name + "New velocity is = " + gameObject.GetComponent<Timeline>().rigidbody.velocity);
        }
    }   
}
