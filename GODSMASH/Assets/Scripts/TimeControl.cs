﻿using UnityEngine;
using System.Collections;
using Chronos;
using UnityEngine.UI;

//this script hanles time Controll And the current score of the player
public class TimeControl : MonoBehaviour {

    public bool timeStopped = false;
    public Text timeDash;

    public float currentScore = 0f;
    public float timeBeforeRound;       //the amount of time that passes before the player starts their run

    public bool playing = false;        //whether or not the score should increase
    //public bool updateScores;           //whether or not we should update the highscore board

    public Clock boxes;

    public HighScores hs;

    void Start()
    {
        boxes = Timekeeper.instance.Clock("Boxes");
    }

    void Update () {
       
        if (playing)
        {
            currentScore = boxes.time - timeBeforeRound;
        }
        else if (!playing)
        {
            timeBeforeRound = Time.time;

            if(currentScore > 0f)
            {
                hs.SetHighScore(currentScore);
                hs.ShowHiScores();
                currentScore = 0f;
            }
        }       

        timeDash.text = currentScore.ToString("F2");

        if (this.GetComponent<WandControl>().touchPadButtonDown)
        {
            print("Time Triggered");
        }

        if (this.GetComponent<WandControl>().touchPadButtonDown && timeStopped == false)
        {
            boxes.localTimeScale = 0f;
            timeStopped = true;
        }

        else if (this.GetComponent<WandControl>().touchPadButtonDown && timeStopped == true)
        {
            boxes.localTimeScale = 1;
            timeStopped = false;
        }
    }

    void PlayerDeath()
    {
        PlayerPrefs.SetString("Player Score", timeDash.text);
    }
}
