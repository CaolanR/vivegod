﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIcountDown : MonoBehaviour
{
    public float timeLeft;

    public Text text;

    public bool timerDestroyed = false;

    public GameObject holster;

    void Update()
    {
        text.text = "" + Mathf.Round(timeLeft);
        timeLeft = holster.GetComponent<holsterHandler>().timer;
        
    }

    //public void CountDown()
    //{
    //    timeLeft -= Time.deltaTime;
    //    //text.text = "" + Mathf.Round(timeLeft);
    //    //if (timeLeft < 0)
    //    //{
    //    //    //holster.GetComponent<holsterHandler>().timerDestroyed = true;
    //    //    //Destroy(gameObject);
    //    //}
    //}
}
