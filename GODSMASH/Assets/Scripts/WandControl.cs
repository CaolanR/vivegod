﻿using UnityEngine;
using System.Collections;
using VRTK;
/*This script registers buttons pressed on the wands of the controllers and turns corresponding bools on and off that other scripts can access*/


public class WandControl : MonoBehaviour {

    

    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    public bool gripButtonDown = false;
    public bool gripButtonUp = false;
    public bool gripButtonPressed = false;

    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    public bool triggerButtonDown = false;
    public bool triggerButtonUp = false;
    public bool triggerButtonPressed = false;

    private Valve.VR.EVRButtonId touchPadButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
    public bool touchPadButtonDown = false;
    public bool touchPadButtonUp = false;
    public bool touchPadButtonPressed = false;

    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;

    // Use this for initialization
    void Start () {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
	}
	
	// Update is called once per frame
	void Update () {
	if (controller == null)
        {
            Debug.Log("Controller not initialized");
            return;
        }

        gripButtonDown = controller.GetPressDown(gripButton);
        gripButtonUp = controller.GetPressUp(gripButton);
        gripButtonPressed = controller.GetPress(gripButton);

        triggerButtonDown = controller.GetPressDown(triggerButton);
        triggerButtonUp = controller.GetPressUp(triggerButton);
        triggerButtonPressed = controller.GetPress(triggerButton);

        touchPadButtonDown = controller.GetPressDown(touchPadButton);
        touchPadButtonUp = controller.GetPressUp(touchPadButton);
        touchPadButtonPressed = controller.GetPress(touchPadButton);

        //print("trigger axis = " + gameObject.GetComponent<ControllerInteractionEventArgs>().buttonPressure);
        
        //if (touchPadButtonDown)
        //{
        //    Debug.Log("Touch button was just Pressed");
        //}
        //if (touchPadButtonUp)
        //{
        //    Debug.Log("Touch button was just Unpressed");
        //}
        //if (touchPadButtonDown)
        //{
        //    Debug.Log("Trigger button was just Pressed");
        //}
        //if (triggerButtonUp)
        //{
        //    Debug.Log("Trigger button was just Unpressed");
        //}DDDDDDDDDDDDDDDDDDDDDD
    }
}
