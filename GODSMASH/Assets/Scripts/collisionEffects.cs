﻿using UnityEngine;
using System.Collections;
using Chronos;

public class collisionEffects : MonoBehaviour
{
    public Vector3 bulStart;
    public Vector3 bulEnd;

    public GameObject smoke;
    private Rigidbody rb;

    public ParticleSystem bulletPs;

    public ParticleSystemRenderer psr;

    public HighScores hs;

    void Start()
    {
        bulStart = gameObject.transform.position;
        smoke = gameObject.transform.GetChild(0).gameObject;
        rb = gameObject.GetComponent<Rigidbody>();
        bulletPs = gameObject.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "PlayAgain")
        {
            hs.replay();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            smoke.transform.SetParent(null);
            //print("Bullet collided = " + contact.otherCollider.gameObject);
            GameObject GO = contact.otherCollider.gameObject;
            if (GO.tag == "gravity" || GO.tag == "-gravity")
            {              
                //psr = GO.GetComponent<ReverseGravity>().psr();                //legacy Particle System
                //psr.enabled = true;                                           //legacy Particle System
                if (GO.GetComponent<Timeline>().globalClockKey != "Boxes")
                {
                    GO.GetComponent<Timeline>().globalClockKey = "Boxes";
                    rb.constraints = RigidbodyConstraints.FreezeAll;
                    StopParticleEmission();

                    if (GO.GetComponent<Timeline>().globalClockKey == "Boxes")
                    {
                        GO.GetComponent<ReverseGravity>().SetMat();
                    }
                }
                else if (GO.GetComponent<Timeline>().globalClockKey == "Boxes")
                {                 
                    GO.GetComponent<VelocityTest>().change = true;           //testing shooting boost to velocity
                    
                    GO.GetComponent<Timeline>().globalClockKey = "Root";
                    //psr.enabled = false;                                      //legacy Particle System
                    rb.constraints = RigidbodyConstraints.FreezeAll;
                    StopParticleEmission();

                    if (GO.GetComponent<Timeline>().globalClockKey == "Root")
                    {
                        GO.GetComponent<ReverseGravity>().SetMat();
                    }
                }
            }
            else if (GO.tag == "wall" || GO.tag == "Untagged" || GO.tag == "projectile")
            {
                rb.constraints = RigidbodyConstraints.FreezeAll;
                StopParticleEmission();
            }
        }
    }

    public void StopParticleEmission()               //destroys the object (badly named)
    {      
            var em = bulletPs.emission;
            em.enabled = false;
        Destroy(this.gameObject, 1f);
    }
}


