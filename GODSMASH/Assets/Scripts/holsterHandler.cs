﻿using UnityEngine;
using System.Collections;

public class holsterHandler : MonoBehaviour {

    public Material holster;
    Color temp;

    public float timer;

    public bool shootingTime = false;

    public GameObject countDownCanvas;
    public GameObject wand;
	// Use this for initialization
	void Start () {
        temp = holster.color;
    }
	
	// Update is called once per frame
	void Update () {

        holster.SetColor("_Color", temp);

        timer = countDownCanvas.GetComponent<UIcountDown>().timeLeft;

        if (timer < 0)
        {
            shootingTime = true;
        }
        if (shootingTime)
        {
            countDownCanvas.SetActive(false);

        }
        

	}

    void OnTriggerEnter (Collider other)
    {
        temp.a = 100f;
        
        print("TRIGGERED");
    }
    void OnTriggerStay(Collider other)
    {
        
        CountDown();
        //print("TRIGGERED");
    }
    void OnTriggerExit(Collider other)
    {
        temp.a = 10f;
        print("UNTRIGGERED");
        timer = 3f;
    }

    public void CountDown()
    {
        timer -= Time.deltaTime;

        if(timer == -3f)
        {
            return;
        }
        //text.text = "" + Mathf.Round(timeLeft);
        //if (timeLeft < 0)
        //{
        //    //holster.GetComponent<holsterHandler>().timerDestroyed = true;
        //    //Destroy(gameObject);
        //}
    }
}
