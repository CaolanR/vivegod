﻿using UnityEngine;
using System.Collections;

public class shootLine : MonoBehaviour {

    public Vector3[] bulArray;

    public Vector3 bulStart;
    public Vector3 bulEnd;

    public LineRenderer lr;

	void Start () {
        bulArray = new[] {bulStart, bulEnd};
        lr = gameObject.GetComponent<LineRenderer>();
        lr.SetPositions(bulArray);
        gameObject.transform.SetParent(null);
	}
	

	void Delete () {
        Destroy(this.gameObject);
	}
}
