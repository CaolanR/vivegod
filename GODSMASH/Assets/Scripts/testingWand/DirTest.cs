﻿using UnityEngine;
using System.Collections;

//This Script makes the gimble look in the same direction as gravity, (parralel to gravity's direction)

public class DirTest : MonoBehaviour {

    public GameObject head;

	void Start () {
	
	}
	
	void Update () {
        Quaternion rot = Quaternion.LookRotation(-Physics.gravity, head.transform.forward); //declares a rotation that looks in the direction of -gravity while it's up axis faces the camera's forward vector
        transform.rotation = rot;    
    }
}
