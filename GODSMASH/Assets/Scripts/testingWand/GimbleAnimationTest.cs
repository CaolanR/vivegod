﻿using UnityEngine;
using System.Collections;

public class GimbleAnimationTest : MonoBehaviour
{

    public Animator centreAnim;
    public Animator middleAnim;
    public Animator outsideAnim;

    public GameObject centreRing;
    public GameObject lookAtRef;

    public float centreScale = 0f;
    public float middleScale = 0f;
    public float outsideScale = 0f;

    public bool centerBool = false;
    public bool middleBool = false;
    public bool outsideBool = false;

    public float centreSpeed = 2f;
    public float middleSpeed = 2f;
    public float outsideSpeed = 2f;
    //higher is slower

    public float forwardAngle;
    public float upwardAngle;

    void Start()
    {

    }


    void Update()
    {
        //centre section

        if (!centerBool)
        {
            centreScale += Time.deltaTime/centreSpeed;

            if (centreScale >= 1)
            {
                centerBool = true;
            }

        }
        else if (centerBool)
        {
            centreScale -= Time.deltaTime/ centreSpeed;

            if (centreScale <= 0f)
            {
                centerBool = false;
            }
        }

        //centreAnim.SetFloat("Blend", centreScale);

        //middle section

        if (!middleBool)
        {
            middleScale += Time.deltaTime / middleSpeed;

            if (middleScale >= 1)
            {
                middleBool = true;
            }

        }
        else if (middleBool)
        {
            middleScale -= Time.deltaTime / middleSpeed;

            if (middleScale <= 0f)
            {
                middleBool = false;
            }
        }

        //middleAnim.SetFloat("Blend", middleScale);

        //outer section

        if (!outsideBool)
        {
            outsideScale += Time.deltaTime / outsideSpeed;

            if (outsideScale >= 1)
            {
                outsideBool = true;
            }

        }
        else if (outsideBool)
        {
            outsideScale -= Time.deltaTime / outsideSpeed;

            if (outsideScale <= 0f)
            {
                outsideBool = false;
            }
        }

        //outsideAnim.SetFloat("Blend", outsideScale);

        //direction calculations:
        //print("is centre facing the same way as gravity? " + Vector3.Dot(centreRing.transform.forward.normalized, Physics.gravity.normalized));

        forwardAngle = Vector3.Angle(centreRing.transform.forward.normalized, lookAtRef.transform.forward.normalized);

        upwardAngle = Vector3.Angle(centreRing.transform.up.normalized, lookAtRef.transform.up.normalized);



        //print (angle = Vector3.Angle(centreRing.transform.TransformPoint(centreRing.transform.localPosition), lookAtRef.transform.localPosition));
        //print ("reference ring direction in world space = " + gameObject.transform.InverseTransformDirection(lookAtRef.transform.localRotation.eulerAngles));
    }
}
//vector3.Dot tells us whether of not two normalized Vectors are facing the same way on a scale from 1 to -1
//Vector3.angle will tell