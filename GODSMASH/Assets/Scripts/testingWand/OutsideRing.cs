﻿using UnityEngine;
using System.Collections;

public class OutsideRing : MonoBehaviour {

    public float yAngle;
    public float lerpPos;
    public float lerpSpeed;

    public GameObject refRing;

    public bool lerp = false;
    public bool anglePos;
    public bool correctRot;

    public Vector3 xRot;
    public Vector3 xCurrentRot;

    public GameObject centreRing;                                           //experimental

    void Update()
    {
        print("yaw angle = " + Vector3.Angle(centreRing.transform.right, refRing.transform.right));//experimental

        yAngle = Vector3.Angle(gameObject.transform.right, -refRing.transform.forward);

        if (!correctRot)
        {
            if (lerp)
            {
                lerpPos += Time.deltaTime * lerpSpeed;

                if (anglePos)
                {
                    gameObject.transform.Rotate(0, Mathf.Lerp(0, yAngle, lerpPos), 0);
                }

                else if (!anglePos)
                {
                    gameObject.transform.Rotate(0, Mathf.Lerp(0, -yAngle, lerpPos), 0);
                }
            }
        }

        if (Vector3.Dot(-gameObject.transform.forward, -refRing.transform.forward) > 0)     //if angle is positive
        {
            anglePos = true;
            correctRot = false;
        }

        else if (Vector3.Dot(-gameObject.transform.forward, -refRing.transform.forward) < 0)    //if angle is negative
        {
            anglePos = false;
            correctRot = false;
        }

        if (yAngle == 0)
        {
            correctRot = true;
            lerp = false;
            lerpPos = 0f;
        }

        else if (yAngle != 0)
        {
            correctRot = false;
        }
    }
}
