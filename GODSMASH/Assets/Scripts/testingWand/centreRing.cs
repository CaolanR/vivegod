﻿using UnityEngine;
using System.Collections;

public class centreRing : MonoBehaviour {

    public float xAngle;                                                                    //the angle between gameObject and the target
    public float lerpPos;                                                                   //the position along the lerp that the gameobject occupies
    public float lerpSpeed;                                                                 //the speed that the lerpPos is multiplied by

    public GameObject refRing;                                                              //the reference object used to gain target information

    public bool lerp = false;                                                               //if true the lerp is active
    public bool anglePos;                                                                   //angle positive, this tells the script in which direction the gameObject should rotate
    public bool correctRot;                                                                 //This tells the object whether or not it should keep trying to rotate

    void Update()
    {
        xAngle = Vector3.Angle(gameObject.transform.forward, refRing.transform.forward);    //this is the difference in degrees between the target x rotation and the current x rotation

        if (!correctRot)                                
        {          
            if (lerp)
            {
                lerpPos += Time.deltaTime * lerpSpeed;                                      //increase the lerpPos float over time

                if (anglePos)
                {
                    gameObject.transform.Rotate(Mathf.Lerp(0, xAngle, lerpPos), 0, 0);      //the game object should ADD the difference in degrees to it's x axis
                }

                else if (!anglePos)
                {
                    gameObject.transform.Rotate(Mathf.Lerp(0, -xAngle, lerpPos), 0, 0);     //the game object should SUBTRACT the difference in degrees to it's x axis
                }               
            }
        }

        if(Vector3.Dot(gameObject.transform.up, -refRing.transform.forward) > 0)            //if the gameObject's up direction faces the same way as the target object's backwards direction:
        {
            anglePos = true;                                                                
            correctRot = false;
        }

        else if (Vector3.Dot(gameObject.transform.up, -refRing.transform.forward) < 0)      //the opposite^^
        {
            anglePos = false;
            correctRot = false;
        }

        if (xAngle == 0)                                                                    //if the diffence in angle is the same then set all the bools to off
        {
            correctRot = true;                                                              //this means the lerp bool is not being looked at
            lerp = false;                                                                   //if it was being identified it would be off because it does not need to be moved
            lerpPos = 0f;                                                                   //resets the lerpPos float back to 0%
        }

        else if(xAngle != 0)                                                                //a change in angle is needed
        {
            correctRot = false;                                                             //let the lerp be evaluated
        }
    }
}
