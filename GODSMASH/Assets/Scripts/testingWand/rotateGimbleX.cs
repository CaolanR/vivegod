﻿using UnityEngine;
using System.Collections;

public class rotateGimbleX : MonoBehaviour {

    public Vector3 localGravDir;
    public Vector3 worldDirCentre;
    public Vector3 worldGravDir;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
        if (Input.GetKey(KeyCode.UpArrow))
        {
            gameObject.transform.Rotate(1,0,0 * Time.deltaTime);
            //print("X pressed");
        }

        localGravDir = transform.parent.transform.parent.InverseTransformDirection(Physics.gravity);
        //the direction of gravity expressed as a local direction of outsideRing
        worldDirCentre = transform.TransformDirection(gameObject.transform.localEulerAngles);
        //the local rotation of the centre ring expressed in world space
        worldGravDir = Physics.gravity.normalized;
    }
}
