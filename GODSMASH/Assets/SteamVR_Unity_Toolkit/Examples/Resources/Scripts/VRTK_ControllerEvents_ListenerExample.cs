﻿using UnityEngine;
using System.Collections;
using VRTK;
using Chronos;

public class VRTK_ControllerEvents_ListenerExample : MonoBehaviour
{

    public Clock blocks;
    public Animator gunAnim;
    public float fl = 0f;

    public bool canload = false;

    public bool touchPadDown = false;
    public bool timeFreeze;

    //start

    private void Start()
    {
        //announces that controller has woken up
        VelocityTest.controllerOn = true;


        if (GetComponent<VRTK_ControllerEvents>() == null)
        {
            Debug.LogError("VRTK_ControllerEvents_ListenerExample is required to be attached to a SteamVR Controller that has the VRTK_ControllerEvents script attached to it");
            return;
        }

        Clock blocks = Timekeeper.instance.Clock("Boxes"); //Caolan, clock instance cache

        //Setup controller event listeners
        GetComponent<VRTK_ControllerEvents>().TriggerPressed += new ControllerInteractionEventHandler(DoTriggerPressed);
        GetComponent<VRTK_ControllerEvents>().TriggerReleased += new ControllerInteractionEventHandler(DoTriggerReleased);

        GetComponent<VRTK_ControllerEvents>().TriggerAxisChanged += new ControllerInteractionEventHandler(DoTriggerAxisChanged);

        GetComponent<VRTK_ControllerEvents>().ApplicationMenuPressed += new ControllerInteractionEventHandler(DoApplicationMenuPressed);
        GetComponent<VRTK_ControllerEvents>().ApplicationMenuReleased += new ControllerInteractionEventHandler(DoApplicationMenuReleased);

        GetComponent<VRTK_ControllerEvents>().GripPressed += new ControllerInteractionEventHandler(DoGripPressed);
        GetComponent<VRTK_ControllerEvents>().GripReleased += new ControllerInteractionEventHandler(DoGripReleased);

        GetComponent<VRTK_ControllerEvents>().TouchpadPressed += new ControllerInteractionEventHandler(DoTouchpadPressed);
        GetComponent<VRTK_ControllerEvents>().TouchpadReleased += new ControllerInteractionEventHandler(DoTouchpadReleased);

        GetComponent<VRTK_ControllerEvents>().TouchpadTouchStart += new ControllerInteractionEventHandler(DoTouchpadTouchStart);
        GetComponent<VRTK_ControllerEvents>().TouchpadTouchEnd += new ControllerInteractionEventHandler(DoTouchpadTouchEnd);

        GetComponent<VRTK_ControllerEvents>().TouchpadAxisChanged += new ControllerInteractionEventHandler(DoTouchpadAxisChanged);
    }

    private void DebugLogger(uint index, string button, string action, ControllerInteractionEventArgs e)
    {
        //Debug.Log("Controller on index '" + index + "' " + button + " has been " + action
        //        + " with a pressure of " + e.buttonPressure + " / trackpad axis at: " + e.touchpadAxis + " (" + e.touchpadAngle + " degrees)");
    }

    private void DoTriggerPressed(object sender, ControllerInteractionEventArgs e)
    {
        //DebugLogger(e.controllerIndex, "TRIGGER", "pressed down", e);
        
    }

    private void DoTriggerReleased(object sender, ControllerInteractionEventArgs e)                             //gunAnim trigger axis released
    {
        //DebugLogger(e.controllerIndex, "TRIGGER", "released", e);
        gunAnim.SetBool("loaded", true);
        gunAnim.SetFloat("triggerDepression", 0f);
        if (gunAnim.GetCurrentAnimatorStateInfo(0).IsTag("cockDown"))
        {
            gunAnim.SetBool("loaded", true);
        }
    }

    private void DoTriggerAxisChanged(object sender, ControllerInteractionEventArgs e)                          //has trigger axis changed
    {
        gunAnim.SetFloat("triggerDepression", e.buttonPressure);

        if (e.buttonPressure == 1f && gunAnim.GetBool("loaded") == true)
        {
            gunAnim.SetTrigger("shot");
        }
    }

    private void DoApplicationMenuPressed(object sender, ControllerInteractionEventArgs e)
    {
        //DebugLogger(e.controllerIndex, "APPLICATION MENU", "pressed down", e);
    }

    private void DoApplicationMenuReleased(object sender, ControllerInteractionEventArgs e)
    {
        //DebugLogger(e.controllerIndex, "APPLICATION MENU", "released", e);
    }

    private void DoGripPressed(object sender, ControllerInteractionEventArgs e)
    {
        //DebugLogger(e.controllerIndex, "GRIP", "pressed down", e);
    }

    private void DoGripReleased(object sender, ControllerInteractionEventArgs e)
    {
        //DebugLogger(e.controllerIndex, "GRIP", "released", e);
    }

    private void DoTouchpadPressed(object sender, ControllerInteractionEventArgs e)                             //has touchpad be pressed?
    {
        Clock blocks = Timekeeper.instance.Clock("Boxes"); //Caolan, clock instance cache

        //DebugLogger(e.controllerIndex, "TOUCHPAD", "pressed down", e);

        touchPadDown = true;

        //stop time:
        if (e.touchpadAxis.x <= .5f && e.touchpadAxis.x >= -.5f && e.touchpadAxis.y <= .5f && e.touchpadAxis.y >= -.5f)
        {
            blocks.localTimeScale = 0f;
            timeFreeze = true;

        }

        //rewind Time:
        if (e.touchpadAxis.x < -.5f)
        {
            blocks.localTimeScale = -1f;
            timeFreeze = true;
        }

        //Resume TIme:
        if (e.touchpadAxis.x > .5f)
        {
            blocks.localTimeScale = 1f;
            timeFreeze = false;
        }
    }

    private void DoTouchpadReleased(object sender, ControllerInteractionEventArgs e)                            //has touchpad been released?
    {
        //DebugLogger(e.controllerIndex, "TOUCHPAD", "released", e);

        touchPadDown = false;
        Clock blocks = Timekeeper.instance.Clock("Boxes"); //Caolan, clock instance cache

        //if (timeFreeze)
        //{
        //    blocks.localTimeScale = 1f;
        //}
    }

    private void DoTouchpadTouchStart(object sender, ControllerInteractionEventArgs e)
    {
        //DebugLogger(e.controllerIndex, "TOUCHPAD", "touched", e);
    }

    private void DoTouchpadTouchEnd(object sender, ControllerInteractionEventArgs e)
    {
        //DebugLogger(e.controllerIndex, "TOUCHPAD", "untouched", e);
    }

    private void DoTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)                         //has touchpad axis changed
    {
        //DebugLogger(e.controllerIndex, "TOUCHPAD", "axis changed", e);
        //Clock blocks = Timekeeper.instance.Clock("Boxes"); //Caolan, clock instance cache
        fl = e.touchpadAxis.x;

        //if (touchPadDown && fl < -.5f)
        //{
        //    blocks.localTimeScale = fl;
        //}

        //if (touchPadDown && fl <= 0)
        //{
        //    blocks.localTimeScale = fl;
        //}

        //if (!touchPadDown)
        //{
        //    blocks.localTimeScale = 1f;
        //}
    }
}

